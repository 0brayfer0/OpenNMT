# OpenNMT-py: Open-Source Neural Machine Translation

[![Build Status](https://travis-ci.org/OpenNMT/OpenNMT-py.svg?branch=master)](https://travis-ci.org/OpenNMT/OpenNMT-py)

Framework : [Pytorch](https://github.com/pytorch/pytorch)
Repo original [OpenNMT](https://github.com/OpenNMT/OpenNMT),
an open-source (MIT) neural machine translation system. It is designed to be research friendly to try out new ideas in translation, summary, image-to-text, morphology, and many other domains.


## Requirements
[Pytorch](https://pytorch.org/)
##### CPU install Windows
Basta con instalar
```cmd
conda install -c peterjc123 pytorch-cpu
```
```cmd
pip install torchtext
```
##### CPU install Linux
Basta con instalar
```cmd
pip install flask
pip install nltk
pip install torch==0.3.1
pip install torchtext==0.2.1

```
Descargar punkt para nltk:
en Python
```python
import nltk
nltk.donwload('punkt')

```

##### GPU install

Se probó en una GTX 1070 con cuda 9.0 y cudnn 7.0
Para este caso usar.
```cmd
conda install -c peterjc123 pytorch cuda90
```
Luego de instalar torch
```cmd
pip install torchtext
```
Algunos videos de instalación [Drive](https://drive.google.com/drive/folders/1syb0oQUJg_LF4LvYeSJv6h-9g425Coeb?usp=sharing)

### Paso 1: Preprocesamiento de la data

Los datos deben estar en archivos paralelos.
Cada archivo debe contener oraciones en un idioma y el otro debe ser la traducción que se desea obtener.
Las oraciones deben estar por linea.
Por ejemplo, si tenemos nuestra carpeta `data/`, los archivos son los siguientes:

* `src-train.txt`
* `tgt-train.txt`
* `src-test.txt`
* `tgt-test.txt`
* `src-val.txt`
* `tgt-val.txt`
 
Los 2 archivos de validación son opcionales, para probar la traducción una vez realizado el entrenamiento,
Se debe ejecutar
```cmd
python preprocess.py -train_src data/src-train.txt -train_tgt data/tgt-train.txt -valid_src data/src-test.txt -valid_tgt data/tgt-test.txt -save_data data/demo
```

Testing files are required and used to evaluate the convergence of the training. It usually contains no more than 5000 sentences.


After running the preprocessing, the following files are generated:

* `demo.train.pt`: serialized PyTorch file containing training data
* `demo.valid.pt`: serialized PyTorch file containing validation data
* `demo.vocab.pt`: serialized PyTorch file containing vocabulary data


Internally the system never touches the words themselves, but uses these indices.

### Step 2: Train the model

```bash
python train.py -data data/demo -save_model demo-model -gpuid 0
```
The main train command is quite simple. Minimally it takes a data file
and a save file.  This will run the default model, which consists of a
2-layer LSTM with 500 hidden units on both the encoder/decoder. You
can also add `-gpuid 1` to use (say) GPU 1.

### Step 3: Translate

```bash
python translate.py -model demo-model_acc_XX.XX_ppl_XXX.XX_eX.pt -src data/src-val.txt -output pred.txt -replace_unk -verbose
```

Now you have a model which you can use to predict on new data. We do this by running beam search. This will output predictions into `pred.txt`.

Modelos pre-entrenados : [Drive](https://drive.google.com/open?id=1XqlrTFyKiquf-vbvig4R32TA2tRaQbD6)

Ejemplo para el modelo actual de ingles a español.
```cmd
python translate.py -model model/en_es2/en_es_acc_54.75_ppl_8.92_e13.pt -src data/en_es/en2.txt -output salida.txt -replace_unk -verbose
```
### Servidor
Requiere del archivo que esta en available_models/example.config.json, se debe especificar la ruta donde senecuetran los modelos y establecerles un ID.
Estan implementado el método POST, para enviar un JSON con las oraciones a traducir y recibir otro JSON con las oraciones traducidas.
Ejecutar :
```cmd
python server.py
```
Para ejecutar una prueba con algun modelo se puede usar postman o curl:
```cmd
curl -X POST -H "Content-Type: application/json" -d '[{"id":100,"src":"My president"}]' http://127.0.0.1:5000/translate
```


!!! note "Note"
    The predictions are going to be quite terrible, as the demo dataset is small. Try running on some larger datasets! For example you can download millions of parallel sentences for [translation](http://www.statmt.org/wmt16/translation-task.html) or [summarization](https://github.com/harvardnlp/sent-summary).

## Pretrained embeddings (e.g. GloVe)

Go to tutorial: [How to use GloVe pre-trained embeddings in OpenNMT-py](http://forum.opennmt.net/t/how-to-use-glove-pre-trained-embeddings-in-opennmt-py/1011)

## Pretrained Models

The following pretrained models can be downloaded and used with translate.py (These were trained with an older version of the code; they will be updated soon).

- [onmt_model_en_de_200k](https://drive.google.com/file/d/0B6N7tANPyVeBWE9WazRYaUd2QTg/view?usp=sharing): An English-German translation model based on the 200k sentence dataset at [OpenNMT/IntegrationTesting](https://github.com/OpenNMT/IntegrationTesting/tree/master/data). Perplexity: 20.
- onmt_model_en_fr_b1M (coming soon): An English-French model trained on benchmark-1M. Perplexity: 4.85.


## Citation

[OpenNMT technical report](https://doi.org/10.18653/v1/P17-4012)

```
@inproceedings{opennmt,
  author    = {Guillaume Klein and
               Yoon Kim and
               Yuntian Deng and
               Jean Senellart and
               Alexander M. Rush},
  title     = {OpenNMT: Open-Source Toolkit for Neural Machine Translation},
  booktitle = {Proc. ACL},
  year      = {2017},
  url       = {https://doi.org/10.18653/v1/P17-4012},
  doi       = {10.18653/v1/P17-4012}
}
```
